import React from 'react';
import {
  Route,
  Redirect,
} from 'react-router-dom';

// TODO: token should be saved in localStorage(maybe) 
let token = localStorage.getItem('XInstaAuthToken');

// ! Это сделано, чтобы возможно было переходить по "защищенным" роутам в development режиме
if (process.env.NODE_ENV === 'development') {
  token = true;
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    token
      ? (<Component {...props} />)
      : (
        <Redirect to={{
          pathname: '/sign-in',
        }} />
      )
  )} />
)

export default PrivateRoute;
