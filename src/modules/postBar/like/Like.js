import React, { Component } from 'react';
import './_like.scss';

class Like extends Component {
  constructor() {
    super();
    this.state = {
      liked: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      liked: !this.state.liked
    });
  }

  render() {
    const label = this.state.liked ?
      <img className='liked' src='https://goo.gl/dcmijf' alt=''/> :
      <img className='unliked' src='https://goo.gl/pGbVLY' alt=''/>
    return (
      <div className="like-wrap">
        <button className="like-button" onClick={this.handleClick}>
          {label}</button>
      </div>
    );
  }
}

export default Like;