import React, { Component } from 'react';
import './button.scss'

class Button extends Component {
  render() {
    let buttonType = 'default';
    if (this.props.buttonType) buttonType = this.props.buttonType;
    
    return (
      <button
        type="submit"
        value="submit"
        className={'btn btn_' + buttonType}>
        {this.props.children}
      </button>
    )
  }
}

export default Button;