import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './_footer.scss';

export default class Footer extends Component {
  render() {
    const iconPath = 'https://cdn1.iconfinder.com/data/icons/' +
            'web-essentials-circle-style/48/add-512.png';
    return (
      <footer className='footer'>
        <div className='footer--inner'>
          <Link to="/new-post">
            <img src={iconPath} alt="LOGO" />
          </Link>
        </div>
      </footer>
    )
  }
}