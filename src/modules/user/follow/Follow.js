import React, { Component } from 'react';
import './_follow.scss';

class Follow extends Component {
  constructor(props) {
    super(props);
    this.state = {follow: true};

    this.follow = this.follow.bind(this);
  }

  follow() {
    this.setState((prevState) => {
      return {follow: !prevState.follow};
    });
  }

  render() {
    return (
      <div className='follow-button-wrap'>
        {this.state.follow ?
          <button onClick={this.follow}
            className='follow-button'>
            follow
          </button> :
          <button onClick={this.follow}
            className='unfollow-button'>
            unfollow
          </button>
        }
      </div>
    );
  }
}

export default Follow;