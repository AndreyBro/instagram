import React, { Component } from 'react';
import Slider from 'react-slick';
import './_postSlider.scss';

class PostSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings} className='post-slider'>
        <div className='post-slider--item'>
          <img src="http://placekitten.com/g/400/300" alt='' />
        </div>
        <div className='post-slider--item'>
          <img src="http://placekitten.com/g/400/300" alt='' />
        </div>
        <div className='post-slider--item'>
          <img src="http://placekitten.com/g/400/300" alt='' />
        </div>
        <div className='post-slider--item'>
          <img src="http://placekitten.com/g/400/300" alt='' />
        </div>
      </Slider>
    );
  }
}

export default PostSlider;