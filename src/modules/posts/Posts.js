import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { fetchPostsAction } from './../../store/actions/fetchPostsAction';
import PostBar from './../postBar/PostBar';
import AddComment from './../postBar/addComment/AddComment';
import PostSlider from './postSlider/PostSlider';
import './_posts.scss';


class Posts extends Component {
  componentDidMount() {
    this.props.fetchPostsAction();
  }

  render() {
    const { posts } = this.props.posts;
    const { comment } = this.props;    

    return (
      <div className='posts'>
        <ul className="posts-list">
          {
            posts && posts.map(post => {
            return (
              <li key={post.id} className='post'>
                <div className='post--inner'>
                  <div className='post-top'>
                    <PostSlider/>
                  </div>
                  <PostBar />
                  <div className='post-content'>
                    <h4 className='post-content--description'>{post.body}</h4>
                    <hr />
                  </div>
                  <div className='post-add-comment'>
                    {(comment) ? <AddComment/> : ''}
                  </div>
                </div>
              </li>
            )
          })
          }
        </ul>
      </div>
    )
  }
}



// у connect есть 2 аргумента.
// 1-й mapStateToProps нам позволяет передать стору, а точнее ее часть posts, в компонент Home
// в devtools мы видим в стору попал posts , а не вся стора
const mapStateToProps = store => {
  return {
    posts: store.posts,
    comment: store.comment
  }
};

// 2-й позволяет передать стору в this.props компонента Home
const mapDispatchToProps = dispatch => ({
  fetchPostsAction: posts => dispatch(fetchPostsAction(posts))
});

// в итоге connect 
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Posts));