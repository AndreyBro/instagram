import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Like from './like/Like';
import { showCommentAction } from './../../store/actions/showCommentAction';
import './_post-bar.scss';


class PostBar extends Component {
  showComments = () => {
    this.props.showCommentAction();
  }
  
  render() {
    return (
      <div className='post-bar'>

        <div className='post-bar--left'>
          <Link to="/user">
            <img src="https://picsum.photos/50/50/?random" alt="" />
          </Link>
          <Link to="/user">
            <span>Bob Gibson</span>
          </Link>
        </div>

        <div className='post-bar--right'>
          <button className='comment-icon-wrap' onClick={this.showComments}>
            <img className='comment-icon' alt=''
              src='https://static.thenounproject.com/png/682476-200.png'>
            </img>
          </button>
          <Like />
        </div>

      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    comment: store.comment
  }
};

const mapDispatchToProps = dispatch => ({
  showCommentAction: comment => dispatch(showCommentAction(comment))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostBar));