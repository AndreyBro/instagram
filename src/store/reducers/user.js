import { SET_YEAR } from './../actions/user';

// 4. создаем первое состояние и записываем его в state
const initialState = {
  name: 'Boby',
  surname: 'Robbinson',
  age: 43,
  avatar: 'https://picsum.photos/150/150/?image=1005',
  description: 'Lorem ipsum dolor, sit amet consectetur  Modi voluptate perspiciatis aperiam.',
}

function userReducer(state = initialState, action) {
  switch (action.type) {
  case SET_YEAR:
    return { ...state, year: action.payload }

  default:
    return state
  }
}

export default userReducer;