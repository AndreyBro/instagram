import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { fetchPostsAction } from './../../store/actions/fetchPostsAction';
import Follow from './follow/Follow';
import './_user.scss';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isLoading: false,
      fullWiew: false
    };
  }

  componentDidMount() {
    this.props.fetchPostsAction();
  }

  setFullWiew = () => this.setState({ fullWiew: true })
  setNoFullWiew = () => this.setState({ fullWiew: false })

  render() {
    const { user } = this.props;
    const { posts } = this.props.posts;
    
    return (
      <div className='user-page'>
        <div className='user-page-profile'>
          <div className='user-page-profile--inner'>
            <div className='user-page-profile--image'>
              <img src={user.avatar} alt="" />
            </div>
            <div className='user-page-profile--description'>
              <h3 className='profile-name'>{user.name} {user.surname}</h3>
              <p className='profile-description'>{user.description}</p>
              <Follow />
            </div>
          </div>
          <hr />
        </div>

        <div className='full-view'>
          <button className='full-view--three' onClick={this.setNoFullWiew}>
            <div></div>
            <div></div>
            <div></div>
          </button>
          <button className='full-view--one'
            onClick={this.setFullWiew}>
            <div></div>
            <div></div>
            <div></div>
          </button>
        </div>

        <div className='posts'>
          <ul className={(this.state.fullWiew === true) ? 
            'posts-list posts-list--one' : 'posts-list posts-list--three' }>
            {
              posts && posts.map(post => {
              return (
                <li key={post.id} className='post-item'>
                  <div className='post-item--inner'>
                    <div className='post-top'>
                      <img className='post-image'
                        src="https://picsum.photos/600/600/?random" alt="" />
                    </div>
                  </div>
                </li>
              )
            })
            }
          </ul>
        </div>
      </div>
    )
  }
}


const putStateToProps = store => {  
  return {
    posts: store.posts,
    user: store.user
  }
};

const mapDispatchToProps = dispatch => ({
  fetchPostsAction: posts => dispatch(fetchPostsAction(posts))
});

export default withRouter(connect(putStateToProps, mapDispatchToProps)(User));