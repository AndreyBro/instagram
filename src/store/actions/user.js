export const SET_YEAR = 'SET_YEAR';

export const fetchUserAction = () => (dispatch) => {
  dispatch({ type: SET_YEAR });
};