import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import userReducer from './user';
import postsReducer from './posts';
import commentReducer from './comment';

export const rootReducer = combineReducers({
  user: userReducer,
  posts: postsReducer,
  form: formReducer,
  comment: commentReducer
})

export default rootReducer;