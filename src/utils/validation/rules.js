import Joi from 'joi';

export const required = value => value ? undefined : 'Required'

export const email = value => {
  const schema = Joi.string().email().label('Email');
  return joiValidation(value, schema);
}

export const pass = value => {
  const schema = Joi.string().min({ limit: 6, label: 'aaa' });
  return joiValidation(value, schema);
}


export const match = matchName => (value, allValues) =>
  allValues.pass !== allValues.confirmPass
    ? 'Not matches to password field' : '';

export const username = value => {
  const schema = Joi.string().alphanum().min(3).max(30).lowercase().label('Login');
  return joiValidation(value, schema);
}

/**
 *
 * @param {*} value
 * @param {<Joi>} schema validation schema of Joi validator
 * @returns
 */
function joiValidation(value, schema) {
  if (!schema || typeof schema !== 'object' || !schema.isJoi) {
    throw Error('Argument \'schema\' must be \'Joi\' validator schema');
  }
  const { error } = Joi.validate(value, schema);
  if (error) return error.message;
  else return null;
}