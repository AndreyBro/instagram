import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import history from './history';
import rootReducer from './reducers/index';
import initialState from './initialState';


// 2. здесь мы создаем store и у нас есть один обязательный параметр rootReducer
const store = createStore(
  connectRouter(history)(rootReducer),
  initialState,
  applyMiddleware(thunk, routerMiddleware(history)),
  compose(
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  )
);

export default store;