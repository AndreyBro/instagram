# Just copypaste project of Instagram

## Before working with project

- Read about main concept of the project structure in 2 related articles: [1](https://jaysoo.ca/2016/02/28/organizing-redux-application/) and [2](https://jaysoo.ca/2016/12/12/additional-guidelines-for-project-structure/#exporting-and-testing-connected-components)

## Run project

For run project on http://localhost:5000:
```
$ npm run start
```

Builds the app for production to the `build` folder
```
$ npm run build
```

## Usefull links

- [Project specifications](https://docs.google.com/document/d/1j44q954jMgoCXzsLTW1nroKZmf7AYH1DCvHWMFIUmzE/edit#)

- [Scrum tracker](https://trello.com/b/m5wtZCzG/insta)

- [Gitlab repo](https://gitlab.com/AndreyBro/instagram/)

- [Git branching model](https://www.slideshare.net/abodeltae/a-successful-git-branching-model-67122143)

- [Routing](https://docs.google.com/document/d/11PFDRCM8Zj1SZgAgMhOQHLR3s6yAHjw8u6MvhG3Nmng/edit)

- [Design](https://drive.google.com/drive/folders/1prffwNyNA3Zc5BchnuQ0YpPYSmLXZRJW?usp=sharing)