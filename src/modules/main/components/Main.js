import React, { Component } from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';
import PrivateRoute from './../../../components/core/PrivateRoute';
import Home from './Home/Home';
import NotFound from './../../404/NotFound';
import User from './../../user/User';
import NewPost from './../../newPost/NewPost';
import forms from './../../forms';


const { LoginForm, SignUpForm } = forms;


export default class Main extends Component {
  render() {
    return (
      <div className='mainContainer'>
        <Switch>
          <PrivateRoute exact={true} path="/" component={Home} />
          <Route exact={true} path="/sign-in" component={LoginForm} />
          <Route exact={true} path="/sign-up" component={SignUpForm} />
          <Route exact={true} path="/new-post" component={NewPost} />
          <Route exact={true} path='/user' component={User} />
          <Route component={NotFound} />
        </Switch>
      </div>
    )
  }
}
