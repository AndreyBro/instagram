import LoginForm from './components/LoginForm/LoginForm';
import SignUpForm from './components/SignUpForm/SignUpForm';

export default {
  LoginForm,
  SignUpForm,
}