const protocol = 'http';
const domain = 'localhost';
const port = 3030;

export default {
  baseUrl: `${protocol}://${domain}:${port}`,
}