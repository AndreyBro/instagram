import React, { Component } from 'react';
import './_addComment.scss';

class AddComment extends Component {
  render() {

    return (
      <form className='form_comment'>
        <div className='form_comment-field'>
          <div contentEditable="true"
            className='form_comment--area'></div>
        </div>
        <div className='form_comment-field'>
          <input type='submit' value='Send' className='form_comment--send' />
        </div>
      </form>
    );
  }
}

export default AddComment;