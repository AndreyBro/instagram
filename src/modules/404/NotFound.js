import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './_notFound.scss';

export default class NotFound extends Component {
  render() {
    return (
      <div className='notFoud'>
        <div className='notFoud--inner'>
          <div className="title" data-content="404">404</div>
          <div className="subtitle">
            Oops, the page you're looking for doesn't exist.
          </div>
          <NavLink to="/" className="notFoud-buttons">
            <div className="notFoud-button">Go to homepage</div>
          </NavLink>
        </div>
      </div>
    )
  }
}