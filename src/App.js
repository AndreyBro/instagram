import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './modules/header/Header';
import main from './modules/main';
import Footer from './modules/footer/Footer';

const { Main } = main;

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Main />
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = store => {
  //console.warn(store);

  return {
    user: store.user,
    posts: store.posts,
    comment: store.comment
  }
};

export default withRouter(connect(mapStateToProps)(App));