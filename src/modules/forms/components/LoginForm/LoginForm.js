import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Redirect } from 'react-router-dom';
import ValidationWarning from '../ValidationWarning';

import {
  required,
  username
}
  from '../../../../utils/validation/rules';
import Form from '../Form/Form';
import coreComponents from '../../../../components/core';

const { Button } = coreComponents;


class LogInForm extends Component {
  state = {
    redirectToReferrer: false
  }

  render() {
    const { handleSubmit, submitting } = this.props;
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer === true) {
      return <Redirect to={from} />
    }

    return (
      <div>
        <Form
          additionalClasses="form_default loginForm"
          onSubmit={handleSubmit}
          title='Log in'>
          {/* Add async checking on username existing */}
          <Field name="username" type="text"
            component={ValidationWarning} label="Login"
            validate={[required, username]} />

          <Field name="pass" type="password"
            component={ValidationWarning} label="Password"
            validate={required} />

          <Button type="submit"
            disabled={submitting}>Submit
          </Button>
        </Form>
      </div>
    )
  }
}

LogInForm = reduxForm({
  form: 'LogInForm'
})(LogInForm);

export default LogInForm;