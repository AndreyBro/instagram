import React, { Component } from 'react';
import './form.scss';

export default class LoginForm extends Component {
  render() {
    return (
      <form className={'form ' + this.props.additionalClasses}>
        <h3 className="form__title">
          {this.props.title}
        </h3>
        {this.props.children}
      </form>
    )
  }
}