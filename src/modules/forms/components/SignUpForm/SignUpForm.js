import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Form from '../Form/Form';
import coreComponents from '../../../../components/core';
import ValidationWarning from '../ValidationWarning';

import {
  email,
  pass,
  match,
  required,
  username
} from '../../../../utils/validation/rules';

const { Button } = coreComponents;


class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
    this.onSubmitLoginBtn = this.onSubmitLoginBtn.bind(this);
  }

  onSubmitLoginBtn(e) {
    e.preventDefault();
    this.handleSignUpBtn = this.handleSignUpBtn.bind(this);
  }

  handleSignUpBtn = e => {
    e.preventDefault();
  }

  render() {
    const { handleSubmit, submitting } = this.props;

    return (
      <Form
        additionalClasses="form_default signUpForm"
        onSubmit={handleSubmit}
        title='Sign Up'>

        <Field name="username" type="text"
          component={ValidationWarning} label="Username"
          validate={[required, username]} />

        <Field name="email" type="email"
          component={ValidationWarning} label="Email"
          validate={[required, email]} />

        <Field name="pass" type="password"
          component={ValidationWarning} label="Password"
          validate={[required, pass]} />

        <Field name="confirmPass" type="password"
          component={ValidationWarning} label="Confirn pass"
          validate={[required, match('')]} />

        <Button type="submit"
          disabled={submitting}>Submit
        </Button>
      </Form>
    )
  }
}


SignUpForm = reduxForm({
  form: 'SignUpForm'
})(SignUpForm);

export default SignUpForm;