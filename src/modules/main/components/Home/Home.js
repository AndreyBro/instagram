import React, { Component } from 'react';
import Posts from './../../../posts/Posts';
import './home.scss';

class Home extends Component {
  render() {    
    return (
      <div className="home">
        <h1 className="page-name">Posts</h1>
        <Posts />
      </div>
    )
  }
}

export default Home;