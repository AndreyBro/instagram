export const SHOW_COMMENT = 'SHOW_COMMENT';

export const showCommentAction = () => (dispatch) => {
  dispatch({ type: SHOW_COMMENT });
};