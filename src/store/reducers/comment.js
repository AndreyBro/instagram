import { 
  SHOW_COMMENT
} from './../actions/showCommentAction';
import initialState from '../initialState';


const commentReducer = function (state = initialState, action) {  
  switch (action.type) {
  case SHOW_COMMENT:
    return true
  default:
    return state;
  }
};


export default commentReducer;