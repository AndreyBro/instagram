import React, { Component } from 'react';
import './_newPost.scss';

class NotFound extends Component {
  render() {
    return (
      <div className='newPost'>
        <h1 className='page-name'>New Post page</h1>
      </div>
    )
  }
}

export default NotFound;