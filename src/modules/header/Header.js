import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import './_header.scss';
import homeIcon from '../../img/icon/homeIcon.png';
import settingsIcon from '../../img/icon/settingsIcon.png';
import profileIcon from '../../img/icon/profileIcon.png';


class Header extends Component {
  render() {
    return (
      <header className="site-header">
        <div className='site-header--inner'>
          <div className='logo'>
            <NavLink to="/">
              <div>
                <img src={homeIcon} alt="HomeIcon" />
              </div>
            </NavLink>
          </div>
          <div className='site-menu'>
            <div className='header-menu'>
              <div className='header-menu-icon'>
                <ul className='header-menu-icon-list'>
                  <li>
                    <Link to="/sign-in">Sign in</Link>
                  </li>
                  <li>
                    <Link to="/sign-up">Sign up</Link>
                  </li>
                </ul>
                <div className='header-menu-icon-img'>
                  <img src={settingsIcon} alt="Settings"></img>
                </div>
              </div>
            </div>
            <NavLink to="/user" className='header-user'>
              <img src={profileIcon} alt="Logo" />
            </NavLink>
          </div>
        </div>
      </header>
    )
  }
}


export default Header;