import PrivateRoute from './PrivateRoute';
import Button from './Button/Button';

export default {
  Button,
  PrivateRoute,
}