import React from 'react';

export default ({ input, label, type, meta: { touched, error, warning } }) => (
  <div className='form__field-wrapper'>
    <input {...input} placeholder={label} type={type} className='form__field-input' />
    {touched && ((error &&
      <span className='form-err'>{error}</span>)
      || (warning && <span className='form-warn'>{warning}</span>))}
  </div>
)