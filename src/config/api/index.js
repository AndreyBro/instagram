import dev from './dev';
import prod from './prod';
import test from './test';
let config;

if (!process.env.NODE_ENV) {
  throw new Error('Вefine process.env.NODE_ENV');
}
switch (process.env.NODE_ENV) {
case 'development':
  config = dev;
  break;
case 'production':
  config = prod;
  break
case 'test':
  config = test;
  break;
default:
  throw new Error('No NODE_ENV')
}

export default config;